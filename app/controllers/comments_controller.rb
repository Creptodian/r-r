class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create]

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      @confirm = "published!"

      redirect_to :back
    else
      @confirm = "Se ha producido un problema"

    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :article_id)
  end
end
