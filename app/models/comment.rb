class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :article
  validates :user_id, presence: true, length: { maximum: 150 }
  validates :content, presence: true, length: { maximum: 1500 }
  validates :article_id, presence: true

  default_scope -> { order(:created_at) }
end
