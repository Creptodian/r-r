class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments, dependent: :destroy

  validates :title, presence: true
  validates :subtitle, presence: true
  validates :content, presence: true
  validates :user_id, presence: true
end
