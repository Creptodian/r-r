// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
window.onload = function (e) {

    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];


    $('#star :radio').change(
        function () {
            $('.puntuacion').html(this.value + ' estrellas' + "<br>" + 'Gracias por su voto');
        }
    )

    $("#button").button();

    $('#autocomplete').autocomplete({
        source: availableTags
    });

    $("#tabs").tabs();


    $(document).ready(function () {
        $("#button").click(function () {
            $("#search-bar").slideToggle('slow');
        });

    });

    $("#loginLink").click(function (event) {
        event.preventDefault();
        $(".loginbox").fadeToggle("fast");
    });
    $(".close").click(function () {
        $(".loginbox").fadeToggle("fast");
    });


    $("#iniciosesion").click( function (e) {

        $.ajax({
            dataType: "json",
            url: "js/fakelogin.php",
            Type: "GET",
            data:
            {
                user: $("#usuario").val(),
                pass: $("#password").val(),
            },
            success: function (data){
                if(data[0]!="error"){
                    $("#loginsystem").html("Hola, " + data[0]);
                }
                else{
                    alert("Datos Incorrectos");
                }
            }
        });
    });


}
