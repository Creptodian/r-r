json.array!(@articles) do |article|
  json.extract! article, :id, :new, :title, :subtitle, :content, :users
  json.url article_url(article, format: :json)
end
