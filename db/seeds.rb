# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create(name: "admin", email: "test@test.com", password: "123456789", password_confirmation: "123456789")


articles = Article.create [
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1},
                              {title: "Lorem Ipsum", subtitle: "dolor sit amet, consectetur adipiscing elit. Nullam...",
                               content: "dolor sit amet, consectetur adipiscing elit. Nullam dolor sit amet, consectetur
                                        adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulladolor sit amet, consectetur adipiscing elit. Nulla",
                               user_id: 1}
                          ]

comments = Comment.create [{
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1},
                           {
                               content: "HOLAHOLA", user_id: 1, article_id: 1}]
