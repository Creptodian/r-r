class NewTables < ActiveRecord::Migration
  def change
    drop_table :comments
    drop_table :articles
    drop_table :users

    create_table :comments do |t|
      t.text :content
      t.references :user, index: true
      t.references :article, index: true
      t.timestamps
    end
    add_index :comments, [:user_id, :article_id, :created_at]

    create_table :articles do |t|
      t.string :title
      t.string :subtitle
      t.text :content
      t.references :user, index: true

      t.timestamps
    end
    add_index :articles, [:user_id, :created_at]


    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest

      t.timestamps
    end
    add_index :users, :email, unique: true

  end
end
