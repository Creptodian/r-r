class AddForeignKeyPls < ActiveRecord::Migration
  def change
    add_foreign_key :comments, :articles, name: :article_id
  end
end
