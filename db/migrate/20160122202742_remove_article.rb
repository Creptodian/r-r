class RemoveArticle < ActiveRecord::Migration
  def up
    drop_table :articles
  end
end
