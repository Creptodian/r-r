class CreateDiceSystems < ActiveRecord::Migration
  def change
    create_table :dice_systems do |t|
      t.string :dice

      t.timestamps
    end
  end
end
